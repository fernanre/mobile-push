import { combineReducers } from "redux";
import ServicesListReducer from "./ServicesListReducer";

const notificationsReducer = combineReducers({
  servicesList: ServicesListReducer
});

export default notificationsReducer;
