import React from "react";
import { View, StyleSheet } from "react-native";
import NotificationsList from "../../notifications/components/NotificationsList";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});

export default props => {
  const { navigation } = props;
  const notifications = navigation.getParam("notifications") || [];
  const updateNotification = navigation.getParam("updateNotification");

  return (
    <View style={styles.container}>
      <NotificationsList
        notifications={notifications}
        updateNotification={updateNotification}
        navigation={navigation}
        {...props}
      />
    </View>
  );
};
