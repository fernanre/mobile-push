import React from "react";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Icon from "react-native-vector-icons/Ionicons";
import NotificationsStack from "./stacks/NotificationsStack";
import SearchStack from "./stacks/SearchStack";
import SettingsStack from "./stacks/SettingsStack";

function getAndroidIcon(iconName, tintColor) {
  return <Icon name={iconName} color={tintColor} size={25} />;
}

const ScreenOptions = iconName => {
  return {
    tabBarIcon: ({ tintColor }) => {
      return getAndroidIcon(iconName, tintColor);
    }
  };
};

const NotificationsListScreenOptions = () => {
  return {
    ...ScreenOptions("md-notifications"),
    tabBarLabel: "Notifications"
  };
};

const CreateRulePageScreenOptions = () => {
  return {
    ...ScreenOptions("md-settings"),
    tabBarLabel: "Rules"
  };
};

const SearchPageOptions = () => {
  return {
    ...ScreenOptions("md-search"),
    tabBarLabel: "Search"
  };
};

const AndroidApp = createMaterialBottomTabNavigator(
  {
    NotificationsListPage: {
      screen: NotificationsStack,
      navigationOptions: NotificationsListScreenOptions
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: CreateRulePageScreenOptions
    },
    Search: { screen: SearchStack, navigationOptions: SearchPageOptions }
  },
  {
    initialRouteName: "NotificationsListPage",
    activeColor: "#fff",
    inactiveColor: "#555",
    barStyle: { backgroundColor: "#2185d0" },
    shifting: true
  }
);

export default AndroidApp;
