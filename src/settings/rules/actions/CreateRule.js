import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../../auth/utils/authUtils";

// Create new notification
export const CREATE_RULE = "CREATE_RULE";
export const CREATE_RULE_SUCCESS = "CREATE_RULE_SUCCESS";
export const CREATE_RULE_FAILURE = "CREATE_RULE_FAILURE";

export const createRule = (rule, type) => {
  return {
    [RSAA]: {
      endpoint: `${BASE_URL}/rules?type=${type}`,
      method: "POST",
      body: JSON.stringify({ rule }),
      credentials: "include",
      headers: withAuth({ "Content-Type": "application/json" }),
      types: [CREATE_RULE, CREATE_RULE_SUCCESS, CREATE_RULE_FAILURE]
    }
  };
};
