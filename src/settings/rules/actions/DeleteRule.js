import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../../auth/utils/authUtils";

// Create new notification
export const DELETE_RULE = "DELETE_RULE";
export const DELETE_RULE_SUCCESS = "DELETE_RULE_SUCCESS";
export const DELETE_RULE_FAILURE = "DELETE_RULE_FAILURE";

export const deleteRule = rulId => {
  return {
    [RSAA]: {
      endpoint: `${BASE_URL}/rules/${rulId}`,
      method: "DELETE",
      credentials: "include",
      headers: withAuth({ "Content-Type": "application/json" }),
      types: [DELETE_RULE, DELETE_RULE_SUCCESS, DELETE_RULE_FAILURE]
    }
  };
};
