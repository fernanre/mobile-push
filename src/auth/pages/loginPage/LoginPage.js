import React from "react";
import { View, Text, Image, Dimensions } from "react-native";
import { Button, Card } from "react-native-elements";

const cernLogo = require("./../../../../assets/CERN-logo_outline.png");

export default ({ navigation }) => {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        height: Dimensions.get("window").height - 80,
        backgroundColor: "white"
      }}
    >
      <Image
        source={cernLogo}
        resizeMode="contain"
        style={{
          height: 100,
          alignSelf: "center"
        }}
      />
      <Card title="CERN Push Notifications">
        <Text
          style={{
            textAlign: "center",
            paddingBottom: 10
          }}
        >
          SignIn with your CERN account to access the CERN Push Notifications
          app.
        </Text>
        <Button
          onPress={() => navigation.navigate("LoginPageWebView")}
          title="Sign in"
        />
      </Card>
    </View>
  );
};
