import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import api from "redux-cached-api-middleware";
import {
  findAllNotifications,
  NOTIFICATIONS_KEY
} from "../../actions/FindAllNotifications";
import NotificationsList from "../../components/NotificationsList";
import * as updateNotificationActionsCreators from "../../actions/UpdateNotification";

const mapStatetoProps = state => {
  return {
    notifications: state.notifications.notificationsList.notifications,
    loading: state.notifications.notificationsList.loading,
    filterFunction: state.notifications.notificationsList.filterFunction
  };
};

const mapDispatchToProps = dispatch => {
  return {
    findAllNotifications: () => dispatch(findAllNotifications),
    ...bindActionCreators(updateNotificationActionsCreators, dispatch),
    clearCache: () =>
      dispatch(
        api.actions.clearCache({
          cacheKey: NOTIFICATIONS_KEY
        })
      )
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(NotificationsList);
