import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as updateFilterNotificationsFunctionActionCreators from "../../actions/UpdateFilterNotificationsFunction";
import FilterNotificationsMenu from "../../components/FilterNotificationMenu";
import * as logoutActionCreators from "../../../auth/actions/Logout";

const mapStatetoProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators(
      updateFilterNotificationsFunctionActionCreators,
      dispatch
    ),
    ...bindActionCreators(logoutActionCreators, dispatch)
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(FilterNotificationsMenu);
