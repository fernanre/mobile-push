import { BASE_URL } from "react-native-dotenv";
import api from "redux-cached-api-middleware";
import { withAuth } from "../../auth/utils/authUtils";

// Get notifications
export const GET_NOTIFICATIONS = "GET_NOTIFICATIONS";
export const GET_NOTIFICATIONS_SUCCESS = "GET_NOTIFICATIONS_SUCCESS";
export const GET_NOTIFICATIONS_FAILURE = "GET_NOTIFICATIONS_FAILURE";
export const NOTIFICATIONS_KEY = "NOTIFICATIONS_KEY";

export const findAllNotifications = api.actions.invoke({
  endpoint: `${BASE_URL}/notifications`,
  method: "GET",
  credentials: "include",
  headers: withAuth({
    "Content-Type": "application/json",
    Accept: "application/json"
  }),
  types: [
    GET_NOTIFICATIONS,
    GET_NOTIFICATIONS_SUCCESS,
    GET_NOTIFICATIONS_FAILURE
  ],
  cache: {
    key: NOTIFICATIONS_KEY,
    strategy: api.cache
      .get(api.constants.CACHE_TYPES.SIMPLE_SUCCESS)
      .buildStrategy()
  }
});
